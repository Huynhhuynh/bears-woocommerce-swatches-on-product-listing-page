# bears-woocommerce-swatches-on-product-listing-page
WooCommerce add on "Woocommerce Variation Swatches and Photos"

![Bears WooCommerce Swatches](http://bearsthemes.com/plugin/images/bears-woocommerce-swatches-on-product-listing-page/Bears-WooCommerce-Swatches.gif "Bears WooCommerce Swatches")

## Options
![Options](http://bearsthemes.com/plugin/images/bears-woocommerce-swatches-on-product-listing-page/options.jpeg "Options")

## Compatible
1. WooCommerce 3.x
2. WooCommerce Variation Swatches and Photos

## Remove By code
```PHP
global $bears_woocommerce_swatches_on_product_listing_page;
remove_action( 'woocommerce_after_shop_loop_item', array($bears_woocommerce_swatches_on_product_listing_page, 'woo_display_product_colors'), 40 );
```

## Hooks
```PHP
add_action('bears_woocommerce_swatches_before_picker_item', ...);
add_action('bears_woocommerce_swatches_after_picker_item', ...);
```
## Style
```SCSS
.theme-extends-woo-pa-swatches-wrap{
  .theme-extends-woo-pa-swatches-item{
    display: block;
    .pa-swatches-label{
      display: inline-block;
      vertical-align: top;
      font-size: 0.71805em;
      text-transform: uppercase;
      font-weight: bold;
      margin-top: 4px;
      margin-right: 4px;
      min-width: 50px;
    }
    .pa-swatches-options{
      display: inline-block;
      vertical-align: top;
      &:after{
        content: "";
        display: block;
        clear: both;
      }
      .pa-swatches-option-item{
        float: left;
        .select-option{
          border-radius: 50px;
          transition: .3s ease;
          -webkit-transition: .3s ease;

        }
        a{
          cursor: pointer;
          width: 18px !important;
          height: 18px !important;
          border-radius: 50px;
          img{
            border-radius: 50px;
          }
        }
      }
    }
  }
}
```
