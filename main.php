<?php
/*
 * Plugin Name: Bears WooCommerce Swatches on product listing page
 * Plugin URI: http://bearsthemes/
 * Description: This plugin an add-on for "woocommerce-variation-swatches-and-photos"
 * Version: 1.0.2
 * Author: Bearsthemes
 * Author URI: http://bearsthemes.com/
 * Text Domain: bears_wooCommerce_swatches
 * WC tested up to: 3.2
 * WC requires at least: 3.0
 *
 * Copyright: © 2015-2017 Bearsthemes.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

 /**
  * Required functions
  */
 if ( ! function_exists( 'woothemes_queue_update' ) ) {
 	require_once( 'woo-includes/woo-functions.php' );
 }

/**
* @since 1.0.0
* Plugin page links
*/
if(! function_exists('bws_product_quick_view_plugin_links')) {
  function bws_product_quick_view_plugin_links( $links ) {

  	$plugin_links = array(
  		'<a href="#">' . __( 'Support', 'bears_wooCommerce_swatches' ) . '</a>',
  		'<a href="#">' . __( 'Docs', 'bears_wooCommerce_swatches' ) . '</a>',
  	);

  	return array_merge( $plugin_links, $links );
  }
  add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'bws_product_quick_view_plugin_links' );
}


if ( is_woocommerce_active() && class_exists( 'WC_Swatches_Attribute_Configuration_Object' ) ) {
	if( ! class_exists('Bears_WooCommerce_Swatches_On_Product_Listing_Page') ) {
		class Bears_WooCommerce_Swatches_On_Product_Listing_Page{

		/**
		 * @since 1.0.0
		 */
		public function __construct() {
			$this->defined();
			$this->inc();

			// Add Settings
			add_filter( 'woocommerce_general_settings' , array( $this, 'settings' ) );

			$this->hooks();
		}

		/**
		 * @since 1.0.0
		 */
		public function defined() {
			define( "BWS_VERSION", '1.0.2' );
			define( "BWS_DIR_URL", plugin_dir_url( __FILE__ ) );
		}

		public function inc() {
			require __DIR__ . '/functions.php';
			require __DIR__ . '/hooks.php';
		}

		/**
		 * settings function.
		 *
		 * @param array $settings
		 */
		public function settings( $settings ) {

			$settings[] = array(
				'name' => __( 'Bears WooCommerce Swatches On Product Listings Page', 'bears_wooCommerce_swatches' ),
				'type' => 'title',
				'desc' => 'The following options are used to configure Bears WooCommerce Swatches extension.',
				'id'   => 'bears_wooCommerce_swatches'
			);

			$settings[] = array(
				'id'      => 'bears_wooCommerce_swatches_label_display',
				'name'    => __( 'Display Label', 'bears_wooCommerce_swatches' ),
				'desc_tip'    => __( 'Display label', 'bears_wooCommerce_swatches' ),
				'type'    => 'select',
				'options' => array(
					'no'        => __( 'No', 'bears_wooCommerce_swatches' ),
					'yes'       => __( 'Yes', 'bears_wooCommerce_swatches' )
				)
			);

			$settings[] = array(
				'id'      => 'bears_wooCommerce_swatches_limit_pa',
				'name'    => __( 'Limit Product Attribute', 'bears_wooCommerce_swatches' ),
				'desc_tip'=> __( 'empty field for show all attribute or input Attribute name want to show (Ex: Color, Size)', 'bears_wooCommerce_swatches', 'bears_wooCommerce_swatches' ),
				'desc'    => __( '(Ex: Color,Size)', 'bears_wooCommerce_swatches' ),
				'type'    => 'text',
			);

			$settings[] = array(
				'type' => 'sectionend',
				'id'   => 'bears_wooCommerce_swatches'
			);

			return $settings;
		}

		/**
		 * @since 1.0.0
		 * Hooks
		 */
		public function hooks() {
			add_action( 'wp_enqueue_scripts', array($this, 'scripts') );
			add_action( 'woocommerce_after_shop_loop_item', array($this, 'woo_display_product_colors'), 40 );

			add_action( 'bears_woocommerce_swatches_before_picker_item', array( $this, 'custom_woocommerce_swatches_before_picker_item' ), 10 );
			add_action( 'bears_woocommerce_swatches_after_picker_item', array( $this, 'custom_woocommerce_swatches_after_picker_item' ), 10 );
		}

		/**
		 * @since 1.0.0
		 */
		public function custom_woocommerce_swatches_before_picker_item($swatch_term) {
			$variation_image_url = $this->available_variations_thumbnail($swatch_term->taxonomy_slug, $swatch_term->term_slug);
			?>
			<div class="pa-swatches-option-item" data-bears-woo-swatches-variation-image="<?php echo esc_attr($variation_image_url); ?>">
			<?php
		}

		/**
		 * @since 1.0.0
		 */
		public function custom_woocommerce_swatches_after_picker_item() {
			?>
			</div>
			<?php
		}


		/**
		 * @since 1.0.0
		 * Scripts
		 */
		public function scripts() {
			wp_enqueue_style( 'bears-woocommerce-swatches', plugins_url( 'assets/css/bears-woocommerce-swatches.css', __FILE__ ), array(), BWS_VERSION );
			wp_enqueue_script( 'bears-woocommerce-swatches', plugins_url( 'assets/js/bears-woocommerce-swatches.js', __FILE__ ), array('jquery'), BWS_VERSION, true );
		}

		/**
		 * @since 1.0.0
		 *
		 */
		public function available_variations_thumbnail($attr_name, $attr_value) {
			global $product;
			$variations = $product->get_available_variations();
			if(empty($variations) || count($variations) <= 0) return;

			$image_id = '';
			$image_url = '';
			foreach($variations as $index => $item) {
			if(isset($item['attributes']['attribute_' . $attr_name]) && $item['attributes']['attribute_' . trim($attr_name)] == trim($attr_value)) {
				$image_id = $item['image_id'];
				break;
			}
			}
			
			if(! empty($image_id)) {
			$image_url = wp_get_attachment_url($image_id, 'shop_catalog');
			return $image_url;
			} else {
			return ;
			}
		}

		/**
		 * @since 1.0.0
		 * // woocommerce_single_variation
		 */
		public function woo_display_product_colors() {
			global $woocommerce, $product, $post;

			$colors = array();
			$swatch_type_options = get_post_meta($product->get_id(), '_swatch_type_options', true);

			if(empty($swatch_type_options)) return;

			$pa = get_post_meta( $product->get_id() , '_product_attributes', true );
			$label_display = get_option( 'bears_wooCommerce_swatches_label_display', 'no' );
			$limit_pa = get_option( 'bears_wooCommerce_swatches_limit_pa', '' );
			$limit_pa_limit = empty( $limit_pa ) ? false : explode( ',', $limit_pa );
			
			if($pa) {
			echo '<div class="theme-extends-woo-pa-swatches-wrap" data-bears-woo-swatches-product="'. $product->get_id() .'">';
			foreach($pa as $key => $pa_item) {
				if(!isset($pa_item['is_variation']) || $pa_item['is_variation'] != true) continue;
				$attribute = $key;
				$config = new WC_Swatches_Attribute_Configuration_Object( $product, $attribute );
				$type = $config->get_type();
				
				# params
				$parmas = array(
					'config' => $config,
					'attribute' => $attribute,
					'pa_data' => array(
						'key' => $key,
						'data' => $pa_item,
					),
					'global_opts' => array(
						'label_display' => $label_display,
						'limit_pa' => $limit_pa_limit,
					)
				);

				/**
				 * 1. bwsopplp_swatch_product_custom_html
				 * 2. bwsopplp_swatch_term_options_html
				 */
				do_action( 'bwsopplp_swatch_'. $type .'_html' , $parmas);
			}
			echo '</div>';
			}
		}
	}

	$GLOBALS['bears_woocommerce_swatches_on_product_listing_page'] = new Bears_WooCommerce_Swatches_On_Product_Listing_Page();
	}
}
