/**
 * @package Bears WooCommerce Swatches
 * @author  bearsthemes
 * @version 1.0.0
 */

!(function(w, $) {
  'use strict';

  /* DOM Ready */
  $(function() {
    $('body').on('mouseover.bears-woocommerce-swatches click.bears-woocommerce-swatches', '[data-bears-woo-swatches-variation-image]', function(e) {
      e.preventDefault();
      var $thisEl = $(this),
          ImageUrl = $thisEl.data('bears-woo-swatches-variation-image');
      
      if(! ImageUrl || ImageUrl == '') return;

      $thisEl.closest('.product').find('.select-option').removeClass('__is-selected');
      $thisEl.find('.select-option').addClass('__is-selected');

      $thisEl
      .closest('.product.has-post-thumbnail')
      .find('img.attachment-woocommerce_thumbnail')
      .attr('src', ImageUrl)
      .attr('srcset', '');
    })
  })
})(window, jQuery)
