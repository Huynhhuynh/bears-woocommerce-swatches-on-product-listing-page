<?php
/**
 * Functions used by plugins
 */
if ( ! class_exists( 'WC_Dependencies_Extension' ) )
	require_once 'class-wc-dependencies-extension.php';

/**
 * WC Detection
 */
if ( ! function_exists( 'is_woocommerce_active' ) ) {
	function is_woocommerce_active() {
		return (WC_Dependencies_Extension::woocommerce_active_check() );
	}
}

if ( ! function_exists( 'is_woocommerce_swatches_active' ) ) {
	function is_woocommerce_swatches_active() {
		return (WC_Dependencies_Extension::woocommerce_swatches_active_check() );
	}
}
