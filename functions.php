<?php 

if(! function_exists('bwsopplp_build_term_label')) {
    /**
     * @ 
     * sanitize_title 
     */
    function bwsopplp_build_term_label( $term_data = array() ) {
        $result = array();
        if( count( $term_data ) <= 0 ) return $result;

        foreach( $term_data as $term_label ) {
            $slug = sanitize_title( trim($term_label) );
            $slug_md5_key = md5( trim($slug) );
            $result[$slug_md5_key] = array(
                'name' => trim($term_label),
                'slug' => trim($slug),
            );
        }

        return $result;
    }
}

if(! function_exists('bwsopplp_find_variable_photo_by_term_slug')) {
    /**
     * @since 1.0.0
     *  
     */
    function bwsopplp_find_variable_photo_by_term_slug( $term_slug = '' ) {
        global $product;
        $product_variable = new WC_Product_Variable( $product );
        $variations = $product_variable->get_available_variations();
    
        $variable_photo_id = 0;
        if( $variations && count( $variations ) > 0 ) {
            foreach( $variations as $item ) {
                if( ! isset( $item['attributes'] ) && count( $item['attributes'] ) == 0 ) continue;
                $slugs = array_values( $item['attributes'] );
                if( in_array( $term_slug, $slugs ) ) {
                    $variable_photo_id = $item['image_id'];
                    break;
                }
            }
        }
    
        return $variable_photo_id;
    }
}

if(! function_exists('bwsopplp_swatch_product_custom_html_func')) {
    /**
     * @since 1.0.0
     *  
     */
    function bwsopplp_swatch_product_custom_html_func( $data = array() ) {
        global $product;
        $product_variable = new WC_Product_Variable( $product );
        $variations = $product_variable->get_available_variations();

        # variables
        $attribute = $data['attribute'];
        $config = $data['config'];
        $pa_data = $data['pa_data'];
        $global_opts = $data['global_opts'];

        $type = $config->get_type();
        $label = wc_attribute_label( $attribute, $product );
        $name = $config->get_attribute_name();
        $options = $config->get_options();
        
        $term_array = explode( '|', $pa_data['data']['value'] );
        $term_label_map = bwsopplp_build_term_label( $term_array );
        // echo '<pre>'; print_r( $variations ); echo '</pre>';
        # ----------------------------

        if( $global_opts['limit_pa'] ) {
            if( ! in_array( $label, $global_opts['limit_pa'] ) ) {
                return;
            }
        }

        ob_start();
        echo ( 'yes' == $global_opts['label_display'] ) ? '<label class="bws-term-label">'. $label .'</label>' : '';
        $num_increase = 0;
        foreach( $options['attributes'] as $md5_key => $attr_item ) {
            $term_label = isset( $term_label_map[$md5_key] ) ? $term_label_map[$md5_key]['name'] : '';

            $variation_thumbnail_url = '';
            $variation_thumbnail_id = bwsopplp_find_variable_photo_by_term_slug( $term_label_map[$md5_key]['name'] ); // $variations[$num_increase]['image_id'];
            $variation_thumbnail_data = wp_get_attachment_image_src($variation_thumbnail_id, 'large');
            if( $variation_thumbnail_data ) { $variation_thumbnail_url = $variation_thumbnail_data[0]; }

            switch( $attr_item['type'] ) {
                case 'image':
                    $image_url = BWS_DIR_URL . '/assets/images/placeholder-image.jpg';
                    if( ! empty( $attr_item['image'] ) ) {
                        $image_data = wp_get_attachment_image_src( $attr_item['image'], $options['size'] );
                        if( $image_data ) { $image_url = $image_data[0]; }
                    }
                    ?>
                    <a href="#" class="bws-item bws-type-image" title="<?php echo esc_attr( $term_label ); ?>" data-bears-woo-swatches-variation-image="<?php echo esc_attr( $variation_thumbnail_url ); ?>">
                        <img src="<?php echo esc_attr( $image_url ); ?>" alt="<?php echo esc_attr( $label ); ?>">
                    </a>
                    <?php
                    break;
                
                case 'color':
                    ?>
                    <a href="#" class="bws-item bws-type-color" title="<?php echo esc_attr( $term_label ); ?>" data-bears-woo-swatches-variation-image="<?php echo esc_attr( $variation_thumbnail_url ); ?>">
                        <span style="background-color: <?php echo esc_attr( $attr_item['color'] ); ?>"></span>
                    </a>
                    <?php
                    break;
            }

            $num_increase += 1;
        }
        $output = ob_get_clean();

        echo implode( '', array(
            '<div class="bws-attributes-container">',
                $output,
            '</div>',
        ) );
    }
}

if(! function_exists('bwsopplp_build_term_options_data')) {
    /**
     * @since 1.0.0
     *  
     */
    function bwsopplp_build_term_options_data( $attribute, $args = array('fields' => 'all') ) {
        global $product;
        
        $terms = wc_get_product_terms( $product->get_id(), $attribute, $args );
        $result = array();

        if( $terms ) {
            foreach( $terms as $term ) {
                $type = get_term_meta($term->term_id, $attribute . '_swatches_id' . '_type', true);
                $photo = get_term_meta($term->term_id, $attribute . '_swatches_id' . '_photo', true);
                $color = get_term_meta($term->term_id, $attribute . '_swatches_id' . '_color', true);
                
                $result[md5($term->slug)] = array(
                    'label' => $term->name,
                    'slug' => $term->slug,
                    'type' => $type, 
                    'photo' => $photo, 
                    'color' => $color
                );
            }
        }

        return $result;
    }
}

if(! function_exists('bwsopplp_swatch_term_options_html_func')) {
    /**
     * @since 1.0.0
     *  
     */
    function bwsopplp_swatch_term_options_html_func( $data = array() ) {
        global $product;
        $product_variable = new WC_Product_Variable( $product );
        $variations = $product_variable->get_available_variations();

        # variables
        $attribute = $data['attribute'];
        $term_data = bwsopplp_build_term_options_data( $attribute );

        $config = $data['config'];
        $options = $config->get_options();
        $global_opts = $data['global_opts'];
        $label = wc_attribute_label( $attribute, $product );
        // print_r( $variations );
        # ----------------------------
        
        if( $global_opts['limit_pa'] ) {
            if( ! in_array( $label, $global_opts['limit_pa'] ) ) {
                return;
            }
        }

        ob_start();
        echo ( 'yes' == $global_opts['label_display'] ) ? '<label class="bws-term-label">'. $label .'</label>' : '';
        $num_increase = 0;
        foreach( $options['attributes'] as $md5_key => $attr_item ) {
            if( isset( $term_data[$md5_key] ) ) {
                // print_r( $term_data[$md5_key] );

                $variation_thumbnail_url = '';
                $variation_thumbnail_id = bwsopplp_find_variable_photo_by_term_slug( $term_data[$md5_key]['slug'] ); // $variations[$num_increase]['image_id'];
                $variation_thumbnail_data = wp_get_attachment_image_src($variation_thumbnail_id, 'large');
                if( $variation_thumbnail_data ) { $variation_thumbnail_url = $variation_thumbnail_data[0]; }
                
                switch( $term_data[$md5_key]['type'] ) {
                    case 'image':
                        $image_url = BWS_DIR_URL . '/assets/images/placeholder-image.jpg';
                        if( ! empty( $attr_item['image'] ) ) {
                            $image_data = wp_get_attachment_image_src( $term_data[$md5_key]['photo'], $options['size'] );
                            if( $image_data ) { $image_url = $image_data[0]; }
                        }
                        ?>
                        <a href="#" class="bws-item bws-type-image" title="<?php echo esc_attr( $term_data[$md5_key]['label'] ); ?>" data-bears-woo-swatches-variation-image="<?php echo esc_attr( $variation_thumbnail_url ); ?>">
                            <img src="<?php echo esc_attr( $image_url ); ?>" alt="<?php echo esc_attr( $term_data[$md5_key]['label'] ); ?>">
                        </a>
                        <?php
                        break;
                    
                    case 'color':
                        ?>
                        <a href="#" class="bws-item bws-type-color" title="<?php echo esc_attr( $term_data[$md5_key]['label'] ); ?>" data-bears-woo-swatches-variation-image="<?php echo esc_attr( $variation_thumbnail_url ); ?>">
                            <span style="background-color: <?php echo esc_attr( $term_data[$md5_key]['color'] ); ?>"></span>
                        </a>
                        <?php
                        break;
                }
    
                $num_increase += 1;
            }
        }
        $output = ob_get_clean();

        echo implode( '', array(
            '<div class="bws-attributes-container">',
                $output,
            '</div>',
        ) );
    }
}
